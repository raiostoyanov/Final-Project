﻿using Image.Data;
using Image.Data.Models;
using Image.Service.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Image.Service.Services
{
    class PhotoJunkieService : IPhotoJunkieService

    {

        private readonly Database _db;

        public PhotoJunkieService(Database db)
        {
            this._db = db;
        }
        public async Task<IEnumerable<Contest>> ContestsActiveOpen()
        {
            var contests = from x in _db.Contests where x.ContestPhase == ContestPhase.PhaseI && x.Category == Data.Models.Enums.Category.Open select x;
            return await contests.ToListAsync();
        }

        public PhotoJunkie Get(int id)
        {

            if ( id == 0)
            {
                return null;
            }
            var photoJunkie =  _db.PhotoJunkies.FirstOrDefault(p => p.UserID == id);
            if (photoJunkie == null)
            {
                return null;
            }
            return photoJunkie;
        }

        public async Task<IEnumerable<PhotoJunkie>> GetAll()
        {
            var junkies = await _db.PhotoJunkies.ToListAsync();
            return junkies;
        }

        public void ParticipateInContest(int id, Contest contest)
        {
            var photoJunkie = _db.PhotoJunkies.FirstOrDefault(p => p.ID == id);
            photoJunkie.Contests.Append(contest);
            contest.Participants.Append(photoJunkie);
        }

        public async Task<IEnumerable<Contest>> ParticipatingContests(User user)
        {
            var photoJunkie = await _db.PhotoJunkies.FirstOrDefaultAsync(p => p.UserID == user.Id);
            return photoJunkie.Contests.ToList();

        }

        public  IEnumerable<Comment> ViewComments(int id)
        {
            var photoJunkie = Get(id);
            return  photoJunkie.Comments.ToList();
        }

        public IEnumerable<Score> ViewScores(int id)
        {
            var photoJunkie = Get(id);
            return photoJunkie.Scores.ToList();
        }

        public Photo GetPhoto(PhotoJunkie photoJunkie, int contestID)
        {   
           var photo=photoJunkie.Photos.Where(p => p.ContestID == contestID);
            return (Photo)photo;
        }
    }
}
