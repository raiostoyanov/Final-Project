﻿using Image.Data;
using Image.Data.Models;
using Image.Service.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services
{
    public class UserServices : IUserServices
    {
        private readonly Database _db;

        public UserServices(Database db)
        {
            this._db = db;
        }

        public async  Task<User> Get(int id)
        {
            var user =await  this._db.Users.FindAsync(id);
            return user;

        }      

        public List<User> GetAll()
        {
            var users = this._db.Users
                            .ToList();
            return users;
        }

        public async Task<User> Create(User user)
        {
            _db.Users.Add(user);
            var photoJunkie = new PhotoJunkie();
            photoJunkie.UserID = user.Id;
            _db.PhotoJunkies.Add(photoJunkie);
            await _db.SaveChangesAsync();
            return user;
        }
    
    }
}
