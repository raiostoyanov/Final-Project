﻿using Image.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services.Contracts
{
    public interface IContestPageServices
    {

        Task<Contest> Get(int? id);

        Task<IEnumerable<Contest>> GetAll();

        Task<Contest> Create(Contest contest);

        Task Edit(Contest contest);

        Task Delete(Contest contest);

        IEnumerable<Photo> SubmittedPhotos(Contest contest);

    }
}
