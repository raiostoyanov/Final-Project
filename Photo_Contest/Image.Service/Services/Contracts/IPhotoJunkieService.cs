﻿using Image.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services.Contracts
{
    public interface IPhotoJunkieService
    {
        Task<IEnumerable<PhotoJunkie>> GetAll ();
        PhotoJunkie Get(int id);
        void ParticipateInContest(int id, Contest contest);

        Task<IEnumerable<Contest>> ContestsActiveOpen();

        Task<IEnumerable<Contest>> ParticipatingContests(User user);

        IEnumerable<Score> ViewScores(int id);

        IEnumerable<Comment> ViewComments(int id);

      
    }
}
