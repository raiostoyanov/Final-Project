﻿using Image.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services.Contracts
{
    public interface IOrganizerService
    {
        Task<IEnumerable<Contest>> ContestsPhaseI();


        Task<IEnumerable<Contest>> ContestsPhaseII();

        Task<IEnumerable<Contest>> ContestsFinished();

        Task<IEnumerable<User>> ViewPhotoJunkies();
       
    }
}
