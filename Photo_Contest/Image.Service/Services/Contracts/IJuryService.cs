﻿using Image.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services.Contracts
{
    public interface IJuryService
    {

        Task<IEnumerable<Jury>> GetAll();


        Task<Jury> Get(int? id);


        IEnumerable<Photo> ViewSubmittedPhotos(Contest contest);


    }
}
