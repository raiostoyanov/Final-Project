﻿using Image.Data;
using Image.Data.Models;
using Image.Data.Models.Enums;
using Image.Service.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services
{
    public class ContestPageServices : IContestPageServices
    {
        private readonly Database _db;

        public ContestPageServices(Database db)
        {
            this._db = db;
        }

        [HttpGet]
        public async Task<Contest> Get(int? id)
        {
            if (id == null || id == 0)
            {
                return null;
            }
            var obj = await _db.Contests.FindAsync(id);
            if (obj == null)
            {
                return null;
            }
            return obj;

        }
        [HttpGet]
        public async Task<IEnumerable<Contest>> GetAll()
        {
            var contests = await this._db.Contests
                            .ToListAsync();
            return contests;
        }
        [HttpPost]
        public async Task<Contest> Create(Contest contest)
        {
            _db.Contests.Add(contest);
            await _db.SaveChangesAsync();
            return contest;
        }
        [HttpPut]
        public async Task Edit(Contest contest)
        {
            _db.Update(contest);
            await _db.SaveChangesAsync();
        }

        [HttpDelete]
        public async Task Delete(Contest contest)
        {
            _db.Remove(contest);
            await _db.SaveChangesAsync();
        }
        
        public IEnumerable<Photo> SubmittedPhotos(Contest contest)
        {
            var photos = contest.Photos.ToList();

            return photos;
        }
    }
}
