﻿using Image.Data;
using Image.Data.Models;
using Image.Service.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services
{
    public class JuryService : IJuryService
    {
        private readonly Database _db;

        public JuryService(Database db)
        {
            this._db = db;
        }

        [HttpGet]
        public async Task<IEnumerable<Jury>> GetAll()
        {
            var jury = await this._db.Juries
                            .ToListAsync();
            return jury;
        }
        [HttpGet]
        public async Task<Jury> Get(int? id)
        {
            
            if (id == null || id == 0)
            {
                return null;
            }
            var jury = await this._db.Juries.FindAsync(id);
            if (jury == null)
            {
                return null;
            }
            return jury;
        }

        public IEnumerable<Photo> ViewSubmittedPhotos(Contest contest)
        {
            var photos=  contest.Photos.ToList();
            return  photos;
        }
    }
}
