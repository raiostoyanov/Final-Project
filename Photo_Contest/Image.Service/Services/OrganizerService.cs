﻿using Image.Data;
using Image.Data.Models;
using Image.Service.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image.Service.Services
{
    public class OrganizerService : IOrganizerService
    {

        private readonly Database _db;

        public OrganizerService(Database db)
        {
            this._db = db;
        }
        /*
        [HttpGet]
        public async Task<User> Get(int? id)
        {
            return NotImplementedException();

        }
        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            return ();
        }*/

        public async Task<IEnumerable<Contest>> ContestsPhaseI()
        {
            var contests = from x in _db.Contests where x.ContestPhase == Image.Data.Models.ContestPhase.PhaseI select x;
            return await contests.ToListAsync();
        }

        public async Task<IEnumerable<Contest>> ContestsPhaseII()
        {
            var contests = from x in _db.Contests where x.ContestPhase == Image.Data.Models.ContestPhase.PhaseII select x;
            return await contests.ToListAsync();
        }
        public async Task<IEnumerable<Contest>> ContestsFinished()
        {
            var contests = from x in _db.Contests where x.ContestPhase == Image.Data.Models.ContestPhase.Finished select x;
            return await contests.ToListAsync();
        }

        public async Task<IEnumerable<User>> ViewPhotoJunkies()
        {
            var junkies = from x in _db.Users where x.Rank == Image.Data.Models.Enums.Rank.Junkie select x;
            return await junkies.ToListAsync();

        }
    }
}
