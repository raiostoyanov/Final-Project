﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Image.Data.Models
{
    public class Jury
    {
        public int ID { get; set; }

        public int UserID { get; set; }
        public User User { get; set; }
        
        public IEnumerable<Comment> Comments { get; set; }

        public IEnumerable<Score> ScoresGiven { get; set; }


    }
}
