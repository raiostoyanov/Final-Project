﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Image.Data.Models
{
    public class Photo
    {
        [Key]
        public int ID { get; set; }

        public string Title { get; set; }

        public string Story { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string file { get; set; }

        public int ParticipantID { get; set; }
        public PhotoJunkie Participant { get; set; }

        public int ContestID { get; set; }
        public Contest Contest { get; set; }
       
    }
}
