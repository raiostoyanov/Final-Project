﻿using Image.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Image.Data.Models
{
    public class Contest
    {
        [Key]
        public int ID { get; set; }
        public ContestPhase ContestPhase { get; set; } = ContestPhase.PhaseI;

        public string Title { get; set; }

        public Category Category { get; set; }

        public DateTime PhaseI { get; set; }
        
        public DateTime PhaseII { get; set; }

        public IEnumerable<PhotoJunkie> Participants { get; set; }


        public IEnumerable<Photo> Photos { get; set; }
    }
}
