﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Image.Data.Models
{
    public class Comment
    {
        public int ID { get; set; }

        public string Text { get; set; }
    }
}
