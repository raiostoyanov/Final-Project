﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Image.Data.Models
{
    public enum  ContestPhase
    {
        
        PhaseI,       
        PhaseII,      
        Finished

    }
}
