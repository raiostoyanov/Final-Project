﻿using Image.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Image.Data.Models
{
    public class PhotoJunkie
    {

        public int ID { get; set; }

        public int UserID { get; set; }
        public User User { get; set; }


        public IEnumerable<Contest> Contests { get; set; }

        public IEnumerable<Photo> Photos { get; set; }

        public IEnumerable<Score> Scores { get; set; }

        public IEnumerable<Comment> Comments { get; set; }

        public int Points { get; set; }
        public Rank Rank { get; set; }
    }
}
