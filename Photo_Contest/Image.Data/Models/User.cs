﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using Image.Data.Models.Enums;

namespace Image.Data.Models
{
    public class User : IdentityUser<int>
    {
        // Hides base property
        // [Key]
        // public int Id { get; set; }

        // Hides base property
        // Required will be configured in OnModelCreating
        // [Required, EmailAddress]
        // public string Email { get; set; }

        // Provided by the base class
        // public int RoleId { get; set; }
        // public Role Role { get; set; }

        public Rank Rank { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

       
    }
}
