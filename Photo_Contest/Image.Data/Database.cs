﻿using Image.Data.Configurations;
using Image.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Image.Data
{
    public class Database : IdentityDbContext<User, Role, int>
    {
        public Database() { }

        public Database(DbContextOptions<Database> options)
            : base(options) { }


        public DbSet<Photo> Photos { get; set; }
        public DbSet<Contest> Contests { get; set; }
        public DbSet<PhotoJunkie> PhotoJunkies { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Jury> Juries { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfig());

            this.Seed(modelBuilder);

            base.OnModelCreating(modelBuilder);

        }

        protected virtual void Seed(ModelBuilder modelBuilder) 
        {
            // Roles
            modelBuilder.Entity<Role>().HasData(
                new Role() { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role() { Id = 2, Name = "User", NormalizedName = "USER" }             
                );
            // Password hasher
            var passHasher = new PasswordHasher<User>();

            // Admin
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.FirstName = "Michael";
            adminUser.LastName = "Tsonkov";
            adminUser.UserName = "admin@admin.com";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = passHasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            modelBuilder.Entity<User>().HasData(adminUser);

            // Link Role & User (for Admin)
            var adminUserRole = new IdentityUserRole<int>();
            adminUserRole.RoleId = 1;
            adminUserRole.UserId = adminUser.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(adminUserRole);

            // Regular User
            var regularUser = new User();
            regularUser.Id = 2;
            regularUser.FirstName = "Alexander";
            regularUser.LastName = "Stoyanov";
            regularUser.UserName = "user@user.com";
            regularUser.NormalizedUserName = "USER@USER.COM";
            regularUser.Email = "user@user.com";
            regularUser.NormalizedEmail = "USER@USER.COM";
            regularUser.PasswordHash = passHasher.HashPassword(regularUser, "user123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            modelBuilder.Entity<User>().HasData(regularUser);

            // Link Role & User (for Regular User)
            var regularUserRole = new IdentityUserRole<int>();
            regularUserRole.RoleId = 2;
            regularUserRole.UserId = regularUser.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole);


            var contests = new List<Contest>()
            {
                new Contest
                {
                   ID=1,
                   Title="Africa Photos",
                   PhaseI= new DateTime(2022, 6, 10),
                   PhaseII= new DateTime(2023, 6, 10)

                }
            };

            modelBuilder.Entity<Contest>().HasData(contests);


        }


    }
}
