﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Image.MVC.Models
{
    public class RegisterViewModel
    {
		[Required]
		[EmailAddress]
		[Display(Name = "E-mail")]
		public string Email { get; set; }

		//[Required]
		[Display(Name = "Username")]
		[StringLength(maximumLength: 20, MinimumLength = 3, ErrorMessage = "The Username must be between 3 and 20 characters long")]
		public string Username { get; set; }

		//[Required]
		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		//[Required]
		[Display(Name = "Last Name")]
		public string LastName { get; set; }


		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		[StringLength(maximumLength: 20, MinimumLength = 3, ErrorMessage = "The password must be between 3 and 20 characters long")]
		public string Password { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Confirm password")]
		[Compare("Password", ErrorMessage = "The password and confirm password fields must match")]
		public string ConfirmPassword { get; set; }

	}
}
