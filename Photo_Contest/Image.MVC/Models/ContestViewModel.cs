﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Image.MVC.Models
{
    public class ContestViewModel
    {
        public string Title { get; set; }       

        public DateTime PhaseITimeLimit { get; set; }

        public DateTime PhaseIITimeLimit { get; set; }

    }
}
