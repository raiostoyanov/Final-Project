﻿using Image.Data.Models;
using Image.MVC.Models;
using Image.Service.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Image.MVC.Controllers
{
    public class ContestController : Controller
    {
        private readonly IContestPageServices _contestPageServices;

        public ContestController( IContestPageServices contestPageServices)
        { 
            this._contestPageServices = contestPageServices;
        }
        public IActionResult Index()
        {
            var contests = _contestPageServices.GetAll();
            return this.View(model: contests);
        }

        [HttpGet]  
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(ContestViewModel obj)
        {
            if (ModelState.IsValid)
            {
                var contest = new Contest();
                contest.Title = obj.Title;
                contest.PhaseI = obj.PhaseITimeLimit;
                contest.PhaseII = obj.PhaseIITimeLimit;
                _contestPageServices.Create(contest);
            }
            return View(obj);
        }
    }
}
